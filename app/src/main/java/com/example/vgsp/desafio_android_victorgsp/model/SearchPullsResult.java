package com.example.vgsp.desafio_android_victorgsp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class SearchPullsResult {

    @SerializedName("items")
    private ArrayList<Repository> repositories;

    @SerializedName("total_count")
    private int totalCount;


    public ArrayList<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(ArrayList<Repository> repositories) {
        this.repositories = repositories;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
