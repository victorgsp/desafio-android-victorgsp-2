package com.example.vgsp.desafio_android_victorgsp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class Repository implements Serializable {

    private String name;
    @SerializedName("owner")
    private User user;
    private String description;
    @SerializedName("stargazers_count")
    private String stargazersCount;
    @SerializedName("forks_count")
    private String forksCount;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(String stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public String getForksCount() {
        return forksCount;
    }

    public void setForksCount(String forksCount) {
        this.forksCount = forksCount;
    }
}
