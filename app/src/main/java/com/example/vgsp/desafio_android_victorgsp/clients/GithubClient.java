package com.example.vgsp.desafio_android_victorgsp.clients;

import com.example.vgsp.desafio_android_victorgsp.model.PullRequest;
import com.example.vgsp.desafio_android_victorgsp.model.Repository;
import com.example.vgsp.desafio_android_victorgsp.model.SearchRepositoriesResult;
import com.example.vgsp.desafio_android_victorgsp.util.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class GithubClient {

    private final HttpRequest httpRequest;
    private static final String URL_REPOSITORY = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";
    private static final String URL_PULLS = "https://api.github.com/repos/%1$s/%2$s/pulls";
    private final Gson gson;
    private static GithubClient instance;

    private GithubClient(){
        httpRequest = new HttpRequest();
        gson = new GsonBuilder().create();
    }

    public static GithubClient getInstance(){
        if (instance == null){
            instance = new GithubClient();
        }
        return instance;

    }
    
    public ArrayList<Repository> findRepositories(int page){
        String response = httpRequest.doRequest(URL_REPOSITORY + page);

        SearchRepositoriesResult searchRepositoriesResult = gson.fromJson(response, SearchRepositoriesResult.class);

        return searchRepositoriesResult != null ? searchRepositoriesResult.getRepositories() : null;
    }

    public ArrayList<PullRequest> findPulls(String repositoryName, String userName){
        String response = httpRequest.doRequest(String.format(URL_PULLS, userName, repositoryName));

        ArrayList<PullRequest> pulls = gson.fromJson(response,
                new TypeToken<ArrayList<PullRequest>>(){}.getType());

        return pulls;
    }
}
