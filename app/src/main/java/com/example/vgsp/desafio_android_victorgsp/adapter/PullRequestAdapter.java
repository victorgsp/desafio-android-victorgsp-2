package com.example.vgsp.desafio_android_victorgsp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vgsp.desafio_android_victorgsp.HomeActivity;
import com.example.vgsp.desafio_android_victorgsp.R;
import com.example.vgsp.desafio_android_victorgsp.model.PullRequest;
import com.example.vgsp.desafio_android_victorgsp.model.Repository;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Victor Guedes on 07/01/2018.
 */

public class PullRequestAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final SimpleDateFormat dateFormatter;
    private List<PullRequest> pullsRequest;
    int lastIndexSearch = 0;
    Context context;


    public PullRequestAdapter(List<PullRequest> pullsRequest, Context context) {
        this.pullsRequest = pullsRequest;
        inflater = LayoutInflater.from(context);
        this.context = context;
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public int getCount() {
        return pullsRequest.size();
    }

    @Override
    public Object getItem(int i) {
        return pullsRequest.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater row = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = row.inflate(R.layout.pulls_request_row, null);

            viewHolder = new ViewHolder();
            viewHolder.mImageViewPullRequestUserPhoto = convertView.findViewById(R.id.imageViewPullRequestUserPhoto);
            viewHolder.mTextViewPullRequestTitle = convertView.findViewById(R.id.textViewPullRequestTitle);
            viewHolder.mTextViewPullRequestBody = convertView.findViewById(R.id.textViewPullRequestBody);
            viewHolder.mTextViewPullRequestOwnerName = convertView.findViewById(R.id.textViewPullRequestOwnerName);
            viewHolder.mTextViewPullRequestDate = convertView.findViewById(R.id.textViewPullRequestDate);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PullRequest pullRequest = pullsRequest.get(position);
        viewHolder.mTextViewPullRequestTitle.setText(pullRequest.getTitle());
        viewHolder.mTextViewPullRequestBody.setText(pullRequest.getBody());
        viewHolder.mTextViewPullRequestOwnerName.setText(pullRequest.getUser().getLogin());
        viewHolder.mTextViewPullRequestDate.setText(dateFormatter.format(pullRequest.getCreateAt()));

        viewHolder.mImageViewPullRequestUserPhoto.setImageBitmap(null);
        if (pullRequest.getUser().getAvatarUrl() != null && !pullRequest.getUser().getAvatarUrl().isEmpty()){
            ImageLoader.getInstance().displayImage(pullRequest.getUser().getAvatarUrl(), viewHolder.mImageViewPullRequestUserPhoto);
        }

        return convertView;
    }

    static class ViewHolder {
        private TextView mTextViewPullRequestTitle;
        private TextView mTextViewPullRequestBody;
        private TextView mTextViewPullRequestOwnerName;
        private TextView mTextViewPullRequestDate;
        private ImageView mImageViewPullRequestUserPhoto;
    }
}


