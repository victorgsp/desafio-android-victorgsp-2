package com.example.vgsp.desafio_android_victorgsp;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.vgsp.desafio_android_victorgsp.adapter.RepositoryAdapter;
import com.example.vgsp.desafio_android_victorgsp.clients.GithubClient;
import com.example.vgsp.desafio_android_victorgsp.model.Repository;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity {

    private ImageLoaderConfiguration imageLoaderConfiguration;
    private RepositoryAdapter mAdapter;

    @ViewById(R.id.repositoryListView)
    ListView repositoryListView;

    @InstanceState
    ArrayList<Repository> repositories;

    @InstanceState
    int page = 1;

    @ItemClick(R.id.repositoryListView)
    void repositoryListItemClicked(Repository clickedRepository) {
        PullRequestsActivity_.intent(this).repository(clickedRepository).start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putSerializable("array",repositories);
        outState.putInt("page",page);
    }

    @AfterViews
    void configListView(){

        if (repositories == null){
            repositories = new ArrayList<Repository>();
            searchRepositories();
        }

        mAdapter = new RepositoryAdapter(repositories, this);
        repositoryListView.setAdapter(mAdapter);
    }

    @AfterViews
    void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_person_black_36dp)
                .showImageOnLoading(R.drawable.ic_person_black_36dp)
                .showImageOnFail(R.drawable.ic_person_black_36dp).cacheInMemory(true).cacheOnDisk(true).build();
        imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(imageLoaderConfiguration);
    }

    @Background
    public void searchRepositories() {
        ArrayList<Repository> repositoriesToAdd = GithubClient.getInstance().findRepositories(page);
        if (repositoriesToAdd != null){
            updateRepositoryList(repositoriesToAdd);
            page++;
        }

    }

    @UiThread
    void updateRepositoryList(ArrayList<Repository> repositories){
        this.repositories.addAll(repositories);
        mAdapter.notifyDataSetChanged();
    }
}
