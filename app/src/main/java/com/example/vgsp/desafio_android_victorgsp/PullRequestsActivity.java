package com.example.vgsp.desafio_android_victorgsp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.vgsp.desafio_android_victorgsp.adapter.PullRequestAdapter;
import com.example.vgsp.desafio_android_victorgsp.adapter.RepositoryAdapter;
import com.example.vgsp.desafio_android_victorgsp.clients.GithubClient;
import com.example.vgsp.desafio_android_victorgsp.model.PullRequest;
import com.example.vgsp.desafio_android_victorgsp.model.Repository;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_pull_requests)
public class PullRequestsActivity extends AppCompatActivity {

    @Extra
    Repository repository;

    @InstanceState
    ArrayList<PullRequest> pullsRequest;

    private PullRequestAdapter mAdapter;

    @ViewById(R.id.pullRequestListView)
    ListView pullRequestListView;

    @AfterViews
    void changeActivityTitle(){
        getSupportActionBar().setTitle(repository.getName());
    }

    @AfterViews
    void configListView(){
        if (pullsRequest == null){
            pullsRequest = new ArrayList<PullRequest>();
            searchPullsRequest();
        }

        mAdapter = new PullRequestAdapter(pullsRequest, this);
        pullRequestListView.setAdapter(mAdapter);
    }

    @Background
    void searchPullsRequest() {
        ArrayList<PullRequest> pullsToAdd = GithubClient.getInstance().findPulls(repository.getName(), repository.getUser().getLogin());
        if (pullsToAdd != null){
            updatePullsList(pullsToAdd);
        }
    }

    @UiThread
    void updatePullsList(ArrayList<PullRequest> pullsRequest){
        this.pullsRequest.addAll(pullsRequest);
        mAdapter.notifyDataSetChanged();
    }

    @ItemClick(R.id.pullRequestListView)
    void pullRequestListItemClicked(PullRequest clickedPullRequest) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(clickedPullRequest.getUrl()));
        startActivity(i);
    }


}
