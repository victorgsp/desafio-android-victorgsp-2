package com.example.vgsp.desafio_android_victorgsp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class HttpRequest {
    public String doRequest(String urlString) {
        HttpURLConnection conection = null;
        InputStream is = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(
                    urlString);
            conection = (HttpURLConnection)
                    url.openConnection();
            conection.connect();

            is = conection.getInputStream();
            reader = new BufferedReader(
                    new InputStreamReader(is));

            return reader.readLine();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (conection != null){
                conection.disconnect();
            }
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null){
                try{
                    is.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}
